﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace VkGenerator.Xml
{
    public record VkXml(Registry Registry)
    {
        public static VkXml Load(string fileName = "vk.xml")
        {
            using var reader = new StreamReader(fileName);
            using var xml = XmlReader.Create(reader);

            var registry = ReadRegisty(xml);
            return new VkXml(registry);
        }

        private static Registry ReadRegisty(XmlReader xml)
        {
            var vk = VkXmlReader.Create(Node<Registry>)
                .Has(Text<TextNode.Comment>)
                .Has(Node<Platforms>, vk => vk
                    .Has(Node<Platform>)
                )
                .Has(Node<Tags>, vk => vk
                    .Has(Node<Tag>)
                )
                .Has(Node<Types>, vk => vk
                    .Has(Text<TextNode.Comment>)
                    .Has(Node<Type>, vk => vk
                        .Has(Text<TextNode.Comment>)
                        .Has(Text<TextNode.Name>)
                        .Has(Text<TextNode.Type>)
                        .Has(Node<Member>, vk => vk
                            .Has(Text<TextNode.Type>)
                            .Has(Text<TextNode.Name>)
                            .Has(Text<TextNode.Enum>)
                            .Has(Text<TextNode.Comment>)
                        )
                    )
                )
                .Has(Node<Enums>, vk => vk
                    .Has(Text<TextNode.Comment>)
                    .Has(Node<Enum>)
                    .Has(Node<Unused>)
                )
                .Has(Node<Commands>, vk => vk
                    .Has(Node<Command>, vk => vk
                        .Has(Node<Proto>, vk => vk
                            .Has(Text<TextNode.Type>)
                            .Has(Text<TextNode.Name>)
                        )
                        .Has(Node<Param>, vk => vk
                            .Has(Text<TextNode.Type>)
                            .Has(Text<TextNode.Name>)
                        )
                        .Has(Node<ImplicitExternSyncParams>, vk => vk
                            .Has(Node<Param>)
                        )
                    )
                )
                .Has(Node<Feature>, vk => vk
                    .Has(Node<Require>, vk => vk
                        .Has(Node<Require.Type>)
                        .Has(Node<Require.Enum>)
                        .Has(Node<Require.Command>)
                        .Has(Text<TextNode.Comment>)
                    )
                )
                .Has(Node<Extensions>, vk => vk
                    .Has(Node<Extension>, vk => vk
                        .Has(Node<Require>, vk => vk
                            .Has(Node<Require.Type>)
                            .Has(Node<Require.Enum>)
                            .Has(Node<Require.Command>)
                            .Has(Text<TextNode.Comment>)
                        )
                    )
                );

            return vk.Read<Registry>(xml);
        }

        private static TNode Text<TNode>(IReadOnlyList<IVkXmlNode> _, IAttributes __, string? text) where TNode : notnull, TextNode
            => (TNode)Activator.CreateInstance(typeof(TNode), text ?? throw new ArgumentNullException(nameof(text)))!;

        private static TNode Node<TNode>(IReadOnlyList<IVkXmlNode> nodes, IAttributes attr, string? _) where TNode : notnull, IVkXmlNode
        {
            var type = typeof(TNode);
            var ctor = type.GetConstructors()[0].GetParameters();
            var ctorArgs = Array.ConvertAll(ctor, p => p.ParameterType == typeof(IReadOnlyList<IVkXmlNode>) ? (object)nodes : Convert.ChangeType(attr[p.Name!], p.ParameterType));

            var node = (TNode)Activator.CreateInstance(type, ctorArgs)!;

            foreach (var property in type.GetProperties().Where(p => p.CanWrite))
            {
                var value = attr.Get(property.Name);
                var propertyType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                if (value is not null)
                    property.SetValue(node, Convert.ChangeType(value, propertyType));
            }

            return node;
        }
    }
}
