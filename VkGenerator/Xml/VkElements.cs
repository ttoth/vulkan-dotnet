﻿using System.Collections.Generic;

namespace VkGenerator.Xml
{
    public abstract record VkXmlNodeCollection(IReadOnlyList<IVkXmlNode> Children) : IVkXmlNode
    {
        public string? Comment { get; init; }
    }

    public record TextNode(string Text) : IVkXmlNode
    {
        public record Name(string Text) : TextNode(Text);
        public record Type(string Text) : TextNode(Text);
        public record Comment(string Text) : TextNode(Text);
        public record Enum(string Text) : TextNode(Text);
    }

    // ------------- Registry ---------------------------
    public record Registry(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    // ------------- Platform ---------------------------
    public record Platforms(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Platform(string Name, string Protect, string Comment) : IVkXmlNode;

    // ------------- Tags ---------------------------
    public record Tags(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Tag(string Name, string Author, string Contact) : IVkXmlNode;

    // ------------- Types ---------------------------
    public record Types(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Type(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children)
    {
        public string? Name { get; init; }
        public string? Category { get; init; }
        public string? Requires { get; init; }
        public string? Alias { get; init; }
        public string? Parent { get; init; }
        public string? ReturnedOnly { get; init; }
        public string? StructExtends { get; init; }
        public bool? AllowDuplicate { get; init; }
    }

    public record Member(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children)
    {
        public string? Values { get; init; }
        public string? Optional { get; init; }
        public bool? NoAutoValidity { get; init; }
        public string? Len { get; init; }
        public string? AltLen { get; init; }
        public bool? ExternSync { get; init; }
        public string? Selection { get; init; }
        public string? Selector { get; init; }
    }

    // ------------- Enums ---------------------------
    public record Enums(IReadOnlyList<IVkXmlNode> Children, string Name) : VkXmlNodeCollection(Children)
    {
        public string? Type { get; init; }
    }

    public record Enum : IVkXmlNode
    {
        public string? Value { get; init; }
        public string? Name { get; init; }
        public string? Alias { get; init; }
        public string? Comment { get; init; }
        public int? BitPos { get; init; }
    }

    public record Unused(string Start) : IVkXmlNode
    {
        public string? Comment { get; init; }
    }

    // ------------- Commands ---------------------------
    public record Commands(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Command(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children)
    {
        public string? SuccessCodes { get; init; }
        public string? ErrorCodes { get; init; }
        public string? Queues { get; init; }
        public string? Name { get; init; }
        public string? Alias { get; init; }
        public string? RenderPass { get; init; }
        public string? CmdBufferLevel { get; init; }
        public string? Pipeline { get; init; }
    }

    public record Proto(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Param(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children)
    {
        public string? Optional { get; init; }
        public string? Len { get; init; }
        public string? ExternSync { get; init; }
        public bool? NoAutoValidity { get; init; }
    }

    public record ImplicitExternSyncParams(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    // ------------- Feature ---------------------------
    public record Feature(IReadOnlyList<IVkXmlNode> Children, string Api, string Name, string Number) : VkXmlNodeCollection(Children);

    public record Require(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children)
    {
        public string? Feature { get; init; }
        public string? Extension { get; init; }

        public record NamedNode(string Name) : IVkXmlNode
        {
            public string? Comment { get; init; }
        }

        public record Type(string Name) : NamedNode(Name);
        public record Command(string Name) : NamedNode(Name);
        public record Enum(string Name) : NamedNode(Name)
        {
            public string? Extends { get; init; }
            public int? ExtNumber { get; init; }
            public int? Offset { get; init; }
            public int? BitPos { get; init; }
            public string? Value { get; init; }
            public string? Alias { get; init; }
            public string? Dir { get; init; }
        }
    }

    // ------------- Extensions ---------------------------
    public record Extensions(IReadOnlyList<IVkXmlNode> Children) : VkXmlNodeCollection(Children);

    public record Extension(IReadOnlyList<IVkXmlNode> Children, string Name, int Number, string Supported) : VkXmlNodeCollection(Children)
    {
        public string? Type { get; init; }
        public string? Author { get; init; }
        public string? Contact { get; init; }
        public string? Requires { get; init; }
        public string? Platform { get; init; }
        public string? SpecialUse { get; init; }
        public string? DeprecatedBy { get; init; }
        public string? ObsoletedBy { get; init; }
        public string? PromotedTo { get; init; }
        public bool? Provisional { get; init; }
        public int? SortOrder { get; init; }
        public string? RequiresCore { get; init; }
    }
}
