﻿using System.Xml;

namespace VkGenerator.Xml
{
    internal static class XmlReaderExtensions
    {
        public static bool IsOnStartElement(this XmlReader xml, string element) => xml.NodeType == XmlNodeType.Element && xml.Name == element;
        public static bool IsEndElement(this XmlReader xml, string element) => xml.NodeType == XmlNodeType.EndElement && xml.Name == element;
    }
}
