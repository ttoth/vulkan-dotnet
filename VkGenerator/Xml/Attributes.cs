﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace VkGenerator.Xml
{
    public interface IAttributes : IReadOnlyDictionary<string, string>
    {
        string? Get(string key);
    }

    internal class Attributes : IAttributes
    {
        private readonly Dictionary<string, string> _attributes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        public void Add(string key, string value) => _attributes.Add(key, value);

        public string this[string key]
        {
            get
            {
                var value = _attributes[key];
                _attributes.Remove(key);
                return value;
            }
        }

        public int Count => _attributes.Count;
        public IEnumerable<string> Keys => _attributes.Keys;
        public IEnumerable<string> Values => _attributes.Values;

        public string? Get(string key)
        {
            _attributes.Remove(key, out var value);
            return value;
        }

        public bool ContainsKey(string key) => _attributes.ContainsKey(key);
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => _attributes.GetEnumerator();
        bool IReadOnlyDictionary<string, string>.TryGetValue(string key, [MaybeNullWhen(false)] out string value) => _attributes.TryGetValue(key, out value);
        IEnumerator IEnumerable.GetEnumerator() => _attributes.GetEnumerator();

        public void Validate()
        {
            if (Count > 0)
            {
                throw new InvalidVkXmlException($"Not used attributes: {string.Join(", ", Keys)}");
            }
        }
    }
}
