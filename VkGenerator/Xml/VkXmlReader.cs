﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace VkGenerator.Xml
{

    public sealed class VkXmlReader
    {
        private readonly Dictionary<string, VkXmlReader> _handlers = new Dictionary<string, VkXmlReader>(StringComparer.Ordinal);
        private readonly string _elementName;
        private readonly Func<IReadOnlyList<IVkXmlNode>, IAttributes, string?, IVkXmlNode> _creator;

        private VkXmlReader(string elementName, Func<IReadOnlyList<IVkXmlNode>, IAttributes, string?, IVkXmlNode> creator)
        {
            _elementName = elementName;
            _creator = creator;
        }

        public static VkXmlReader Create<TNode>(Func<IReadOnlyList<IVkXmlNode>, IAttributes, string?, TNode> creator) where TNode : notnull, IVkXmlNode
            => new VkXmlReader(typeof(TNode).Name.ToLower(), (nodes, attr, text) => creator(nodes, attr, text));

        public VkXmlReader Has<TNode>(Func<IReadOnlyList<IVkXmlNode>, IAttributes, string?, TNode> creator, Action<VkXmlReader>? configure = null) where TNode : notnull, IVkXmlNode
        {
            var name = typeof(TNode).Name.ToLower();
            var reader = new VkXmlReader(name, (nodes, attr, text) => creator(nodes, attr, text));
            _handlers.Add(name, reader);
            configure?.Invoke(reader);
            return this;
        }

        public TNode Read<TNode>(XmlReader xmlReader) where TNode : notnull, IVkXmlNode
        {
            while (!xmlReader.IsOnStartElement(_elementName)
                && xmlReader.Read()) ;

            var attributes = new Attributes();
            if (xmlReader.HasAttributes)
            {
                for (int i = 0; i < xmlReader.AttributeCount; ++i)
                {
                    xmlReader.MoveToAttribute(i);
                    attributes.Add(xmlReader.Name, xmlReader.Value);
                }
                xmlReader.MoveToElement();
            }

            if (xmlReader.IsEmptyElement)
            {
                var node = (TNode)_creator(Array.Empty<IVkXmlNode>(), attributes, null);
                attributes.Validate();
                return node;
            }

            string? text = null;
            var elements = new List<IVkXmlNode>();

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.EndElement:
                        if (!xmlReader.IsEndElement(_elementName))
                            throw InvalidVkXmlException.UnexpectedNode(_elementName, xmlReader);

                        var node = (TNode)_creator(elements, attributes, text);
                        attributes.Validate();
                        return node;

                    case XmlNodeType.Text:
                        text = xmlReader.Value;
                        elements.Add(new TextNode(text));
                        break;

                    case XmlNodeType.Element:
                        if (!_handlers.TryGetValue(xmlReader.Name, out var handler))
                            throw InvalidVkXmlException.UnexpectedNode(_elementName, xmlReader);

                        elements.Add(handler.Read<IVkXmlNode>(xmlReader));
                        break;
                }
            }

            throw InvalidVkXmlException.CouldNotFindEndElement(_elementName);
        }
    }
}
