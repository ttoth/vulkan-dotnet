﻿using System;
using System.Runtime.Serialization;
using System.Xml;

namespace VkGenerator.Xml
{
    [Serializable]
    public class InvalidVkXmlException : Exception
    {
        public InvalidVkXmlException() : base("Invalid xml file.")
        {
        }

        public InvalidVkXmlException(string message) : base(message)
        {
        }

        public InvalidVkXmlException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidVkXmlException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public static InvalidVkXmlException CouldNotFindElement(string element)
        {
            return new InvalidVkXmlException($"Could not find element '{element}'");
        }

        public static InvalidVkXmlException CouldNotFindEndElement(string element)
        {
            return new InvalidVkXmlException($"Could not find end of element '{element}'");
        }

        public static InvalidVkXmlException UnexpectedNode(string parent, XmlReader xml)
        {
            return new InvalidVkXmlException($"Unexpected element when reading {parent}: {xml.Name}({xml.NodeType})");
        }
    }
}
